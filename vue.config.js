module.exports = {
  lintOnSave: false,
  devServer: {
    proxy: {
      '/api': {
        changeOrigin: true,
        target: "http://106.12.79.128:1901",
        pathRewrite: {
          '^/api': ""
        }
      }
    }
  }
}
