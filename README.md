# 管理系统

## 补齐依赖
```
yarn install
```

### 启动项目
```
yarn run serve
```

### 打包
```
yarn run build
```

### 测试
```
yarn run test
```


